class CreateBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string :title
      t.string :autorIme
      t.string :autorPrezime
      t.text :oknjizi
      t.text :odlomak
      t.text :bio
      t.string :cena
      t.string :isbn
      t.string :prevod
      t.string :pogovor
      t.string :potpis
      t.string :unajavi
      t.string :visina
      t.string :sirina
      t.string :klapna
      t.string :broj_strana
      t.string :broj_strana_odlomak
      t.string :orig_ime
      t.string :kontakt
      t.string :wiki
      t.string :goodreads
      t.text :mailtext
      t.text :bio_lekt

      t.timestamps
    end
  end
end
