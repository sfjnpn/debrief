class CreateEmailadds < ActiveRecord::Migration[5.1]
  def change
    create_table :emailadds do |t|
      t.string :address
      t.string :name

      t.timestamps
    end
  end
end
