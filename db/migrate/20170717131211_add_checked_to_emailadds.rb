class AddCheckedToEmailadds < ActiveRecord::Migration[5.1]
  def change
    add_column :emailadds, :checked, :boolean
  end
end
