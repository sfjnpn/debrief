Rails.application.routes.draw do
  get 'simple_sessions/create'
  post 'simple_sessions/clear'

#  get 'emailadd/sendbyemail'
#  post 'emailadds/sendbyemail'
  post 'books/notify'
  post 'writers/update'

  get 'books/new'
#  resources :books

  post 'books', to: 'books#new'
  patch 'books/:id/', to: 'books#update'
#  post 'books/send', to: 'books#sendbyemail'
  post '/login', to: 'simple_sessions#create'
  get 'home/index'
  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
