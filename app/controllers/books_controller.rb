class BooksController < ApplicationController

  def new
    if logged_in?
      book = Book.create(new_book_params)
      flash.now[:alert] = 'nova knjiga'
    else
      flash[:alert] = 'login timed out'
    end
    redirect_to :root
  end

  #pomoćna f-ja: '*' -> '<i>'
  def italicize(strin)
    while tmp=(/\*[^\*]*\*/).match(strin)
      tmp=tmp.to_s
      rep="<i>" + tmp.gsub(/\*/,"") + "</i>"
      strin.sub!(tmp,rep)
    end
    strin
  end

  def notify
    if  params[:tekst] != "" && logged_in?
      @mails = Emailadd.pluck(:address, :id, :name)
      receps = Array.new
      str=""
      @mails.each do |m|
        e=Emailadd.find(m[1])
        if params[m[2]] || params[:sender][m[2]]
          str+=m[0]
          receps.push(m[0])
          e.checked = true
        else
          e.checked = false
        end
        e.save
      end
      UpdateMailer.notify_email(receps, params[:tekst], params[:sabdzekt]).deliver_now
      flash[:alert]="sent"
      redirect_to :root
    else
      flash[:alert]="no notification content or login timed out"
      redirect_to :root
    end
  end

  #update i send mail
  def update
    if logged_in?
      @book = Book.find(params[:id])
      stra = @book.bio
      strb = params[:book][:bio]
      if stra != strb && !stra.nil?
        lekt = lektorize(stra,strb) #poređenje verzija biografije
        @book.bio_lekt = lekt
        @book.save
      end
      if @book.update(book_params)

        if params[:email] == "send by e-mail"
          book = Book.last
          str = ""
          filename = book.title + " - " + book.autorPrezime + ".txt"
          book.attributes.each do |name, value|
            book[name] ||= ""
            book[name]=italicize(book[name].to_s)
          end
          str = str + "category: knjige\nlayout: default\nedicija: \npermalink: \n"
          str = str + "naslov: \"" + book.title + "\"\n"
          str = str + "autorIme: \"" + book.autorIme + "\"\n"
          str = str +"autorPrezime: \"" + book.autorPrezime + "\"\n"
          str = str + "cena: \"" + book.cena + ",00RSD\"\n"
          str = str + "h: \"" + book.visina + "\"\n"
          str = str + "wCover: \"" + book.sirina + "\"\n"
          str = str + "klapna: \"" + book.klapna + "\"\nhrbat: \" \"\n"
          str = str + "otkriveno: \"<p>žanr: Priče</p>\n"
          if book.prevod != ""
            str = str + "<p>prevod: " + book.prevod + "</p>\n"
          end
          str = str + "<p>pogovor: " + book.pogovor + "</p>\n"
          str = str + "<p>broj strana: "+ book.broj_strana + "</p>\n<p>povez: meki</p>\n"
          str = str + "<p>dimenzije (cm): "+book.visina+"x"+book.sirina+" </p>\n<p>godina izdanja: 2017</p>\n"
          str = str +" <p>ISBN: "+ book.isbn + "</p>\"\n"
          str = str + "oknjizi: " + "\"<p>" + book.oknjizi + "</p> <p class='potpis'>" + book.pogovor + "</p>\"\n"
          str = str + "oautoru:" + "\"<p>" + book.bio + "</p>\"\n"
          str = str + "odlomak:" + "\"<p>" + book.odlomak + "</p>\"" + "<p><strong>Prva priča: </strong> <a href='https://www.dropbox.com/s/jkkg73lhryldgwt/basaid.pdf?raw=1'><i>Glad</i></a></p>"

          #     File.write(filename, str)
          @book.update_attributes(:mailtext => str)
          flash[:alert] = 'sent & saved'
          UpdateMailer.updated_email.deliver_now
          #     File.delete(filename)
          redirect_to :root
        else
          flash[:alert] = 'saved'
          redirect_to :root
        end

      else
        flash.now[:alert] = 'save unsuccessful'
        redirect_to :root
      end
    else
      flash[:alert] = 'login timed out'
      redirect_to :root
    end
  end

  private
  def book_params
    params.require(:book).permit(:oknjizi, :odlomak, :prevod, :pogovor, :potpis, :isbn, :cena, :sirina, :visina, :klapna, :broj_strana,:orig_ime, :broj_strana_odlomak, :unajavi, :bio, :wiki, :goodreads )
  end
  def new_book_params
    params.require(:book).permit(:title, :autorIme, :autorPrezime)
  end

end
