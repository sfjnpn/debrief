class SimpleSessionsController < ApplicationController
  def create
    user = User.find_by(name: "a")
    if user && user.authenticate(params[:session][:password])
      log_in(user)
      redirect_to '/'
    end
  end

  def clear
    if logged_in?
       SimpleSession.where("created_at < ?", 24.hours.ago).delete_all
       redirect_to '/'
    end
  end
end
