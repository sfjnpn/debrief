class Book < ApplicationRecord
 #validates :title, presence: true
 validates :autorIme, presence: true
 validates :autorPrezime, presence: true

 before_validation :strip_whitespace

 private
 def strip_whitespace
   if self.title.respond_to?('strip')
     self.title = (self.title.strip).humanize
   else
     self.title = "oh, pear"
   end
   if self.autorIme.respond_to?('strip')
     self.autorIme = (self.autorIme.strip).titleize
   end
   if self.autorPrezime.respond_to?('strip')
     self.autorPrezime = (self.autorPrezime.strip).titleize
   end    
 end
end
