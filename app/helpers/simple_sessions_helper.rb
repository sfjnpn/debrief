module SimpleSessionsHelper
  def log_in(user)
    begin
      sessid = SecureRandom.hex
    end while SimpleSession.exists?(:session_id => sessid)
    SimpleSession.create(session_id: sessid, user_id: user.id)
    cookies.encrypted[:sessid] = sessid
  end
  def logged_in?
    value = nil
    sess = SimpleSession.find_by(session_id: cookies.encrypted[:sessid])
    if sess
      value = sess.created_at > 24.hours.ago
    end
    value
  end  

end
