module BooksHelper
  #prikaz promena u bio - bold
  def boldicize(strin)
    strin = sanitize(strin)
    pat = Regexp.new("\#\#[^\#]*\#\#")
    while tmp=pat.match(strin)
      tmp=tmp.to_s
      rep="<strong>" + tmp.gsub(/\#\#/,"") + "</strong>"
      strin.sub!(tmp,rep)
    end

    pat1 = Regexp.new("\!\![^\!]*\!\!")
    while tmp=pat1.match(strin)
      tmp=tmp.to_s
      rep="<span style='color:red'>" + tmp.gsub(/\!\!/,"") + "</span>"
      strin.sub!(tmp,rep)
    end
    pat = Regexp.new("\\n")
    rep="<br>"
    strin.gsub!(pat,rep)
    strin
  end

  def newlinicize(strin)
    pat = Regexp.new("\\n")
    rep="<br>"
    strin.gsub!(pat,rep)
    strin
  end


#detekcija razlika između dve verzije teksta
  def lektorize(strin1, strin2)
    def compare_bloks(bloks, i, iplus, outarr, option=false)
      if i+iplus < bloks.length
        b_next = bloks[i+iplus]
        if !(b_next[1]+b_next[2] > bloks[i][1]+bloks[i][2])
          if option
            outarr[outarr.length-1] = [i,i+iplus]
          else
            outarr.push([i,i+iplus])
          end
          compare_bloks(bloks, i, iplus+1, outarr, true)
        end
      end
    end


    def compare_bloks_rev(bloks, i, iplus, outarr, option=false)
      if i+iplus < bloks.length
        b_next = bloks[i+iplus]
        if !(b_next[1]+b_next[2] < bloks[i][1]+bloks[i][2])
          if option
            outarr[outarr.length-1] = [i,i+iplus]
          else
            outarr.push([i,i+iplus])
          end
          compare_bloks_rev(bloks, i, iplus+1, outarr, true)
        end
      end
    end

    a_in = strin1
    b_in = strin2

    a = a_in.split(" ")
    b = b_in.split(" ")

    a_deltas = Array.new(a.length, false)
    b_deltas = Array.new(b.length, false)
    a_repeats = Array.new(a.length,0)

    a_flags = Array.new(a.length, true)
    b_flags = Array.new(b.length, true)
    a_select = Array.new(a.length, true)
    b_select = Array.new(b.length, true)

    # trazi ponavljanja u a
    a.each_with_index do |ael, ai|
      a[ai+1..a.length].each_with_index do |ael2, ai2|
        if ael == ael2
          a_repeats[ai]+=1
        end
      end
    end

    # u nizu b se traze poklapanja sa elementima u a
    a.each_with_index do |ael, ai|
      flag = true
      b.each_with_index do |bel, bi|
        if ael == bel && b_flags[bi]
          a_deltas[ai] = bi-ai
          b_flags[bi]=false
          break
        end
      end
    end

    # u nizu a se traze poklapanja sa elementima u b
    b.each_with_index do |bel, bi|
      flag = true
      a.each_with_index do |ael, ai|
        if ael == bel && a_flags[ai]
          b_deltas[bi] = bi-ai
          a_flags[ai]=false
          break
        end
      end
    end




    a_idx_select = Array.new

    a_deltas.each_with_index do |ael, ai|
      if ael
        a_idx_select.push([ael,ai])
      end
    end


    bloks = Array.new
    prev_delta = a_deltas[0]
    blok_length = 0
    a_deltas.each_with_index do |d, i|
      if d == prev_delta
        blok_length += 1
      else
        bloks.push([blok_length, prev_delta, i-1])
        prev_delta = d
        blok_length = 1
      end
      if i == a_deltas.length-1
        bloks.push([blok_length, prev_delta, i])
      end
    end

    parovi = Array.new
    bloks = bloks.select { |b| b[1] }
    bloksT = bloks[0..(bloks.length-2)]
    bloksT.each_with_index do |b, i|
      compare_bloks(bloks,i,1,parovi)
    end

    parovi_rev = Array.new
    bloks.reverse!
    bloksT = bloks[0..(bloks.length-2)]
    bloksT.each_with_index do |b, i|
      compare_bloks_rev(bloks,i,1,parovi_rev)
    end


    parovi_rev.map!{|p| [(p[1]-bloks.length+1).abs,(p[0]-bloks.length+1).abs]}
    parovi_rev.reverse!
=begin
    parovi.each_with_index do |p,i|
      if (p[0]-p[1]).abs - (parovi_rev[i][0]-parovi_rev[i][1]).abs > 0
        parovi[i] = parovi_rev[i]
        parovi[i][1] -= 1
      else
        parovi[i][0] += 1
      end
    end
=end

    bloks.reverse!
    parovi.each do |p|
      p[0] += 1
      bloks[p[0]..p[1]].each do |blok|
        for it in 0..blok[0]-1
          a[blok[2]-it] = "##" + a[blok[2]-it] + "##"
          b[blok[2]+blok[1]-it] = "##" + b[blok[2]+blok[1]-it] + "##"
        end
      end
    end

    parovi_rev.each do |p|
      p[1] -= 1
      bloks[p[0]..p[1]].each do |blok|
        for it in 0..blok[0]-1
          a[blok[2]-it] = "!!" + a[blok[2]-it] + "!!"
          b[blok[2]+blok[1]-it] = "!!" + b[blok[2]+blok[1]-it] + "!!"
        end
      end
    end


    a_deltas.each_with_index do |ad, i|
      if !ad
        a[i] = "##" + a[i] + "##"
      end
    end

    b_deltas.each_with_index do |bd, i|
      if !bd
        b[i] = "##" + b[i] + "##"
      end
    end
    stra = a.join(" ")
    strb = b.join(" ")
    out = "previous:\n" + stra + "\n\ncurrent:\n" + strb
  end
end
