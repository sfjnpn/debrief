class UpdateMailer < ApplicationMailer
  default from: 'notifications@example.com'
 
  def updated_email
    require 'tempfile'
    @book = Book.last
    file = Tempfile.new('tmp')
    file.write(@book.mailtext)
    file.rewind
    attachments[@book.title + @book.autorPrezime+".txt"] = file.read
    receps = ['eschaton.map@gmail.com', 'sonja.ja.si@gmail.com']
    mail(to: receps, subject: @book.title + @book.autorPrezime)
    file.close
    file.unlink
  end

  def notify_email(email_receps, email_content, email_subject)
    @content = email_content
    str = "pk notification"
    str = email_subject unless email_subject == ""
    mail(to: email_receps, subject:str)  
  end  
end
